#!/usr/bin/env bash
# RackN Copyright 2021
# Build Manager Demo

set -e

PATH=$PATH:.

rm -f profiles/linode.yaml
rm -f profiles/aws.yaml
rm -f profiles/google.yaml

FORCE="false"
while getopts ":P:f" CmdLineOpts
do
  case $CmdLineOpts in
    P) PASSWORD=${OPTARG}     ;;
    f) FORCE="true"          ;;
    u) usage; exit 0          ;;
    \?)
      echo "Incorrect usage.  Invalid flag '${OPTARG}'."
      exit 1
      ;;
  esac
done

export RS_ENDPOINT=$(terraform output drp_manager | tr -d '"')
echo "Using RS_ENDPOINT=$RS_ENDPOINT and RS_KEY=$RS_KEY"

echo "======= CLEANING UP EDGE CLUSTERS ========"
clusters=$(drpcli clusters list Endpoint Ne "" | jq -r .[].Uuid)
for c in $clusters
do
  echo "Cleanup Cluster $c"
  drpcli clusters cleanup $c > /dev/null
done
sleep 1

if [[ "$FORCE" == "true" ]]; then
  echo "WARNING: potential orphaned servers!! skipping WorkflowComplete test"
else
  echo "Making sure all clusters are WorkflowComplete"
  while [[ $(drpcli clusters count WorkflowComplete Eq false) -gt 0 ]]; do
    suspects=$(drpcli clusters list WorkflowComplete Eq false | jq -r .[].Name)
    echo "... waiting 5 seconds.  Working Clusters are [$suspects]"
    sleep 5
  done
  echo "done waiting"
fi

echo "======= CLEANING UP MANAGER CLUSTERS ========"
clusters=$(drpcli clusters list | jq -r .[].Uuid)
for c in $clusters
do
  echo "Cleanup Cluster $c"
  drpcli clusters cleanup $c > /dev/null
done
sleep 1

if [[ "$FORCE" == "true" ]]; then
  echo "WARNING: potential orphaned servers!! skipping WorkflowComplete test"
else
  echo "Making sure all clusters are WorkflowComplete"
  while [[ $(drpcli clusters count WorkflowComplete Eq false) -gt 0 ]]; do
    suspects=$(drpcli clusters list WorkflowComplete Eq false | jq -r .[].Name)
    echo "... waiting 5 seconds.  Working Clusters are [$suspects]"
    sleep 5
  done
  echo "done waiting"
fi

if [ "$FORCE" == "true" ] || [ $(drpcli machines count machine/type Eq virtual) -eq 0 ]; then

  echo "no VMs found.  removing manager"
  terraform init -no-color
  terraform destroy -no-color -auto-approve -var-file=manager.tfvars

  if [[ -e "multi-site-demo.json" ]]; then
    rm multi-site-demo.json
  fi
  rm -f multi-site/profiles/*

  rm -f terraform.tfstate terraform.tfstate.backup
else
  echo "WARNING provisioned virtual machines still exist - did not destroy manager.  Call with -f to force!"
fi


